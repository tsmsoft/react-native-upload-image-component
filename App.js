import React, {useState, useEffect} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    View,
    TouchableOpacity,
    Alert,
    Image
} from 'react-native';

import Color from './src/constants/Colors';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ImagePicker from "./src/components/ImagePicker";

const App = () => {

    const {random} = Math;

    const [images, setImages] = useState([]);

    const [isModalVisible, setIsModalVisible] = useState(false);


    useEffect(() => {
        setDefaultImages();
    }, [])


    const setDefaultImages = () => {
        let views = [];
        for (let i = 0; i < 4; i++) {
            views.push(
                <View style={styles.imageWrapper} key={random().toString()}>
                    <View style={styles.image}>
                        <Icon name="image" size={32} color={Color.GRAY}/>
                    </View>
                </View>)
        }

        setImages(views);
    }


    const fillImages = (selectedImages) => {
        let views = [];
        views = selectedImages.map(image => {
            return (
                <View style={styles.imageWrapper} key={random().toString()}>
                    <View style={styles.image}>
                        <Image source={{uri: image.uri}} style={{width: '100%', height: 100}} />
                    </View>
                </View>
            )
        })

        setImages(views)
    }

    return (
        <SafeAreaView style={styles.container}>


            <View style={styles.imagesContainer}>
                {images}
            </View>

            <TouchableOpacity
                style={styles.buttonWrapper}
                onPress={() => {
                    setIsModalVisible(true)
                }}
            >
                <Icon name="camera" size={40} color={Color.DARK_GRAY}/>
            </TouchableOpacity>

            <ImagePicker
                isModalVisible={isModalVisible}
                setIsModalVisible={setIsModalVisible}
                setDefaultImages={setDefaultImages}
                fillImages={fillImages}
            />


        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imagesContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageWrapper: {
        width: '25%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5
    },
    image: {
        width: '100%',
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderStyle: 'dashed',
        borderRadius: 2,
        borderColor: Color.LIGHT_GRAY
    },
    buttonWrapper: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 15
    }
});

export default App;

import React, {useState} from 'react';
import {Modal, View, Text, StyleSheet, TouchableOpacity, Dimensions, Platform, Linking, Alert} from 'react-native';
import Color from '../constants/Colors';
import Button from "./button";
import PropTypes from 'prop-types';
import {check, request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import ImageCropPicker from 'react-native-image-crop-picker';

const {width: WIDTH, height: HEIGHT} = Dimensions.get('window');

const ImagePicker = (props) => {

    const {isModalVisible, setIsModalVisible, setDefaultImages, fillImages} = props;


    const _takePhoto = () => {

        let permission = Platform.OS === "android" ? PERMISSIONS.ANDROID.CAMERA : PERMISSIONS.IOS.CAMERA;

        check(permission)
            .then(result => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        Alert.alert("Opps", "You don't have camera!");
                        break;
                    case RESULTS.DENIED:
                        request(permission)
                            .then(requestResult => {

                            })
                            .catch(requestError => {

                            })
                        break;
                    case RESULTS.GRANTED:

                        ImageCropPicker.openCamera({
                            compressImageQuality: 0.5,
                            crop: true
                        })
                            .then(image => {

                                let imageInfo = {
                                    uri: Platform.OS === 'ios' ? 'file:///' + image.path : image.path,
                                    type: image.mime
                                }

                                fillImages([imageInfo]);

                            })
                            .catch(imageError => {
                                Alert.alert("Error", "An error has occurred when taking photo!");
                            })

                        break;
                    case RESULTS.BLOCKED:
                        Alert.alert(
                            "Oppss",
                            "You have to set permission manually!",
                            [
                                {
                                    text: "Cancel",
                                    style: "cancel"
                                },
                                {
                                    text: "OK",
                                    onPress: () => Linking.openSettings()
                                }
                            ]
                        )
                        break;
                }

            })
            .catch(error => {

            })

    }


    const _choosePhoto = () => {

        let permission = Platform.OS === "android" ? PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE : PERMISSIONS.IOS.PHOTO_LIBRARY;

        check(permission)
            .then(result => {
                switch (result) {
                    case RESULTS.DENIED:
                        request(permission)
                            .then(requestResult => {

                            })
                            .catch(requestError => {

                            })
                        break;
                    case RESULTS.GRANTED:

                        ImageCropPicker.openPicker({
                            multiple: true,
                            maxFiles: 4,
                            compressImageQuality: 0.5,
                            crop: true
                        })
                            .then(images => {
                                let imageInfos = [];
                                imageInfos = images.map(image => {
                                    return {
                                        uri: Platform.OS === 'ios' ? 'file:///' + image.path : image.path,
                                        type: image.mime
                                    }
                                })
                                fillImages(imageInfos);
                            })
                            .catch(imageError => {
                                Alert.alert("Error", "An error has occurred when taking photo!");
                            })

                        break;
                    case RESULTS.BLOCKED:
                        Alert.alert(
                            "Oppss",
                            "You have to set permission manually!",
                            [
                                {
                                    text: "Cancel",
                                    style: "cancel"
                                },
                                {
                                    text: "OK",
                                    onPress: () => Linking.openSettings()
                                }
                            ]
                        )
                        break;
                }

            })
            .catch(error => {

            })
    }


    return (
        <Modal
            transparent={true}
            visible={isModalVisible}
        >
            <View style={styles.container}>

                <View style={styles.outerContent}>
                    <View style={styles.content}>
                        <View style={styles.modalHeaderWrapper}>
                            <TouchableOpacity onPress={() => {
                                setIsModalVisible(false)
                            }}>
                                <View style={styles.closeButton}>

                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.textWrapper}>
                            <Text style={styles.biggerText}>
                                Upload Image
                            </Text>

                            <Text style={styles.smallText}>
                                Only 4 images can be uploaded!
                            </Text>
                        </View>


                        <View style={styles.buttonWrapper}>

                            <Button
                                buttonText="Take Photo"
                                onPress={() => {
                                    _takePhoto();
                                }}
                            />

                            <Button
                                buttonText="Choose From Gallery"
                                onPress={() => {
                                    _choosePhoto();
                                }}
                            />

                            <Button
                                buttonText="Cancel"
                                onPress={() => {
                                    setDefaultImages();
                                }}
                            />

                        </View>

                    </View>
                </View>
            </View>

        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.2)'
    },
    content: {
        width: '100%',
        height: HEIGHT / 2,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        backgroundColor: Color.WHITE,
        position: 'absolute',
        bottom: 0
    },
    outerContent: {
        width: '100%',
        height: (HEIGHT / 2) + 4,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        backgroundColor: Color.GRAY,
        position: 'absolute',
        bottom: 0
    },
    modalHeaderWrapper: {
        width: '100%',
        height: '10%',
        paddingVertical: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    closeButton: {
        width: 50,
        height: 10,
        borderRadius: 5,
        backgroundColor: Color.LIGHT_GRAY
    },
    textWrapper: {
        width: '100%',
        height: '20%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    biggerText: {
        width: '100%',
        paddingVertical: 5,
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        color: Color.LIGHT_GRAY
    },
    smallText: {
        width: '100%',
        paddingVertical: 5,
        fontSize: 14,
        textAlign: 'center',
        color: Color.LIGHT_GRAY
    },
    buttonWrapper: {
        width: '100%',
        height: '70%',
        paddingHorizontal: 15,
        justifyContent: 'center',
        alignItems: 'center'
    }
});


ImagePicker.propTypes = {
    isModalVisible: PropTypes.bool,
    setIsModalVisible: PropTypes.func.isRequired,
    setDefaultImages: PropTypes.func.isRequired
}

ImagePicker.defaultProps = {
    isModalVisible: false
}

export default ImagePicker;

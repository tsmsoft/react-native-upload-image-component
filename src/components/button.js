import React from 'react';
import {TouchableOpacity, Text, StyleSheet, View} from 'react-native'
import Color from '../constants/Colors';
import PropTypes from 'prop-types';

const Button = (props) => {

    const {buttonText, onPress} = props;

    return (

        <View style={{...styles.buttonWrapper, ...props.buttonWrapperStyle}}>
            <TouchableOpacity
                onPress={onPress}>
                <Text style={styles.buttonText}>
                    {buttonText}
                </Text>
            </TouchableOpacity>
        </View>

    )
}

const styles = StyleSheet.create({
    buttonWrapper: {
        width: '100%',
        height: 45,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.DARK_RED,
        marginVertical: 10
    },
    buttonText: {
        width: '100%',
        textAlign: 'center',
        fontSize: 22,
        color: Color.WHITE,
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: 'bold'
    }
});


Button.propTypes = {
    buttonText: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
}

export default Button;
